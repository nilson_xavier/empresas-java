package com.myimdb.myimdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyimdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyimdbApplication.class, args);
	}

}
