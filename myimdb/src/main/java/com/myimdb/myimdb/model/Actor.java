/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myimdb.myimdb.model;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author nilson
 */
@Entity
public class Actor {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    
    private Set<Movie> filmography;
    
    Role playedIn(Movie movie, String role) { 
        
        Role actorRole = new Role();
        
        actorRole.setMovie(movie);
        actorRole.setRole(role);
        actorRole.setActor(this);
        
        return actorRole;
    }
}
