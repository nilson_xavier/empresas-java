/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myimdb.myimdb.model;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author nilson
 */
@Entity
public class User {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String login;
    private String name;
    private String password;
    
    private Set<Rate> ratings;
    private Set<User> friends;
    
    Rate rate(Movie movie, int stars, String comment) { 
    
        Rate rate = new Rate();
        
        rate.setMovie(movie);
        rate.setStars(stars);
        rate.setComment(comment);
        rate.setUser(this);
        
        return rate;
    }
    
    void befriend(User user) { 
        
        this.friends.add(user);
    }
}
