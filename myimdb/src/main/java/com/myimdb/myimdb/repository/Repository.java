/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myimdb.myimdb.repository;

import com.myimdb.myimdb.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author nilson
 */
public interface Repository extends JpaRepository {
   
    Movie getMovieById(String id);
    
    Movie getMovieByName(String name);
}
