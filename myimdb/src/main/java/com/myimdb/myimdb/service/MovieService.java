/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myimdb.myimdb.service;

import com.myimdb.myimdb.model.Movie;
import com.myimdb.myimdb.exception.MovieDuplicatedException;
import com.myimdb.myimdb.exception.MovieNotFoundException;
import java.util.List;

/**
 *
 * @author nilson
 */

public interface MovieService {
 
    Movie save(Movie movie) throws MovieDuplicatedException;
    Movie update(Movie movie) throws MovieNotFoundException;
    Long delete(Long id) throws MovieNotFoundException;
    List<Movie> findAll();
}
